
# simpleconsul
    import "simpleconsul"

Package simpleconsul provides an easy way to interact with [Consul](https://www.consul.io/) (for example to use [fabio](https://github.com/eBay/fabio) as zero-conf http router)


### Example

```Go
import sc  "bitbucket.org/armitage/simpleconsul"
...
addr := "127.0.0.1:8080"
name := "ServiceName"
tags := []string{"urlprefix-example.com/service"}
serviceID := "service-8080"

cons, err := sc.NewSimpleConsul(
	addr,
	name,
	serviceID,
	tags,
	sc.SetHealthInterval("5s"),
	sc.SetHealthTimeout("5s"))

err := cons.Register()

if err != nil {
	log.Panicln(err)
}
```

If you use [fabio](https://github.com/eBay/fabio) you could connect to your service now via example.com/service without any manual configuration in Consul or fabio.


## func SetHealthHandler
``` go
func SetHealthHandler(hh func(http.ResponseWriter, *http.Request)) func(*SimpleConsul)
```
SetHealthHandler sets the handler for the consul health check.
If you do not provide any, the following default is used:


	func HealthDefaultHandler(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(w, "OK")
	}

the handler is registered with


	http.HandleFunc(...)


## func SetHealthInterval
``` go
func SetHealthInterval(iv string) func(*SimpleConsul)
```
SetHealthInterval sets the interval for consul to perform the health check
defaults to "1s"


## func SetHealthPrefix
``` go
func SetHealthPrefix(prefix string) func(*SimpleConsul)
```
SetHealthPrefix sets the URL Prefix under which the consul could reach the health check of your go program
if you does not set a prefix, the default "/health" is used


## func SetHealthTimeout
``` go
func SetHealthTimeout(ht string) func(*SimpleConsul)
```
SetHealthTimeout sets the timeout for consul to for the health check
defaults to "1s"



## type SimpleConsul
``` go
type SimpleConsul struct {
    // contains filtered or unexported fields
}
```








### func NewSimpleConsul
``` go
func NewSimpleConsul(addr string, name string, serviceID string, tags []string, options ...func(*SimpleConsul)) (*SimpleConsul, error)
```
NewSimpleConsul creates a new Consul Client

addr is the listen adress of your go programm with format


	127.0.0.1:8000

name ist the name of your service in consul
serviceID is the serviceID in consul
tags is an []string for the tags in consul


	tags:=[]string{"tag1","tag2"}









