//Package simpleconsul provides an easy way to interact with Consul (for example to use fabio as reverse proxy)
package simpleconsul

import (
	"fmt"
	"net"
	"net/http"
	"strconv"

	"github.com/hashicorp/consul/api"
)

type SimpleConsul struct {
	addr            string
	name            string
	host            string
	port            int
	tags            []string
	service         *api.AgentServiceRegistration
	healthURL       string
	healthInterval  string
	healthTimeout   string
	healthHandler   func(rw http.ResponseWriter, req *http.Request)
	healthURLPrefix string
	client          *api.Client
}

//NewSimpleConsul creates a new Consul Client
//
//addr is the listen adress of your go programm with format
//		127.0.0.1:8000
//name ist the name of your service in consul
//serviceID is the serviceID in consul
//tags is an []string for the tags in consul
//		tags:=[]string{"tag1","tag2"}
//
func NewSimpleConsul(addr string, name string, serviceID string, tags []string, options ...func(*SimpleConsul)) (*SimpleConsul, error) {

	// get host and port as string/int
	host, portstr, err := net.SplitHostPort(addr)
	if err != nil {
		return nil, err
	}
	port, err := strconv.Atoi(portstr)
	if err != nil {
		return nil, err
	}
	sc := SimpleConsul{
		addr:            addr,
		name:            name,
		host:            host,
		port:            port,
		tags:            tags,
		healthInterval:  "1s",
		healthTimeout:   "1s",
		healthURLPrefix: "/health",
	}
	sc.healthHandler = sc.healthDefaultHandler

	for _, option := range options {
		option(&sc)
	}

	sc.healthURL = "http://" + addr + sc.healthURLPrefix

	sc.service = &api.AgentServiceRegistration{
		ID:      serviceID,
		Name:    name,
		Port:    port,
		Address: host,
		Tags:    tags,
		Check: &api.AgentServiceCheck{
			HTTP:     sc.healthURL,
			Interval: sc.healthInterval,
			Timeout:  sc.healthTimeout,
		},
	}

	sc.client, err = api.NewClient(api.DefaultConfig())
	if err != nil {
		return nil, err
	}

	http.HandleFunc(sc.healthURLPrefix, sc.healthDefaultHandler)

	return &sc, nil
}

//Register is used to register your services with consul
func (s *SimpleConsul) Register() error {
	if err := s.client.Agent().ServiceRegister(s.service); err != nil {
		return err
	}
	return nil
}

//Deregister is used to unregister your services in consul
func (s *SimpleConsul) Deregister() error {
	if err := s.client.Agent().ServiceDeregister(s.service.ID); err != nil {
		return err
	}
	return nil
}

//SetHealthPrefix sets the URL Prefix under which the consul could reach the health check of your go program
//if you does not set a prefix, the default "/health" is used
func SetHealthPrefix(prefix string) func(*SimpleConsul) {
	return func(s *SimpleConsul) {
		s.setHealthPrefix(prefix)
	}
}
func (s *SimpleConsul) setHealthPrefix(prefix string) {
	s.healthURLPrefix = prefix
}

//SetHealthInterval sets the interval for consul to perform the health check
//defaults to "1s"
func SetHealthInterval(iv string) func(*SimpleConsul) {
	return func(s *SimpleConsul) {
		s.setHealthInterval(iv)
	}
}
func (s *SimpleConsul) setHealthInterval(iv string) {
	s.healthInterval = iv
}

//SetHealthTimeout sets the timeout for consul to for the health check
//defaults to "1s"
func SetHealthTimeout(ht string) func(*SimpleConsul) {
	return func(s *SimpleConsul) {
		s.setHealthTimeout(ht)
	}
}
func (s *SimpleConsul) setHealthTimeout(ht string) {
	s.healthTimeout = ht
}

//SetHealthHandler sets the handler for the consul health check.
//If you do not provide any, the following default is used:
//
//		func HealthDefaultHandler(w http.ResponseWriter, r *http.Request) {
//			fmt.Fprintln(w, "OK")
//		}
//
//the handler is registered with
//		http.HandleFunc(...)
//
func SetHealthHandler(hh func(http.ResponseWriter, *http.Request)) func(*SimpleConsul) {
	return func(s *SimpleConsul) {
		s.setHealthHandler(hh)
	}
}
func (s *SimpleConsul) setHealthHandler(hh func(http.ResponseWriter, *http.Request)) {
	s.healthHandler = hh
}

func (s *SimpleConsul) healthDefaultHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "OK")
}
